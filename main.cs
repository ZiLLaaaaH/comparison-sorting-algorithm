long[] inputArray = new long[1000];

Random rnd = new Random();

for (int i = 0; i < inputArray.Length; i++)
{
    inputArray[i] = rnd.Next();
}

private void Swap(ref long valOne, ref long valTwo)
{
    valOne = valOne + valTwo;
    valTwo = valOne - valTwo;
    valOne = valOne - valTwo;
}

private void SwapWithTemp(ref long valOne, ref long valTwo)
{
    long temp = valOne;
    valOne = valTwo;
    valTwo = temp;
}